# Some slides on Bayesian Sampling
This first series of slides has been initially prepared for the yearly
[POLARIS bootcamp](https://team.inria.fr/polaris/polaris-bootcamp-may-2019/).

# Compiling
These slides rely on org-mode, beamer, pygmentize, R (and STAN), ...
I'll set up CI asap. In the mean time you can try to compile them like
this:
```
make bayesian_statistics_introduction.tex
make bayesian_statistics_introduction.pdf
```

# Things to do

- Introduce the bayesian reasonning with a discrete Head/Tail coin
  (two possible coins H/T and H/H, one is picked at random, you
  observe H;H;H;H;H;H;H;H, whats the likelihood of having either one
  or the other ? how is it influenced by the prior ?)
- Illustrate the bayesian probability/proportion estimation with the
  water/land detection animation
  https://twitter.com/rlmcelreath/status/1447516072236683265?t=Dr32Sxm3DTYbZn6DHllK1g&s=09
